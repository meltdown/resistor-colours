defmodule Ui.State.Resistor do
  alias Ui.State.Resistor

  @enforce_keys [:value, :selected, :included]
  defstruct [:value, :selected, :included]

  def list_from_seed(seed, width, height) do
    # Construct a list of resistor maps, keyed by index
    resistors =
      # Convert each character to an int value in `Resistor` structs
      Stream.map(String.to_charlist(seed), &(%Resistor{value: &1, selected: false, included: false}))
      # Chunk into columns
      |> Stream.chunk_every(height)
      # Add an index so we can mark some `Resistor`s as included
      |> Stream.map(&(Stream.with_index(&1)))
      # Swap the index and resistor round
      |> Stream.map(&(Stream.map(&1, fn {resistor, index} -> {index, resistor} end)))
      # Take enough columns for our desired width
      |> Stream.take(width)
      # Turn it into a list of maps keyed by index
      |> Stream.map(&(Enum.into(&1, %{})))

    # Select the highest and lowest values and mark them as included
    resistors
      # Get the highest and lowest values
      |> Stream.map(fn group ->
          # If we just use `min_max_by` then there is a chance we get the same element twice (if the seed happens to
          # have the same letter for this entire group), as `min_max_by` returns the first (i.e. the same) match for
          # both `min` and `max` in that case. To counteract this we get `min_by` then reverse the list and get `max_by`
          # as even if the letter is repeated the first matching element will then be different.
          min = Enum.min_by(group, fn {_index, resistor} -> resistor.value end)
          group = Enum.reverse(group)
          max = Enum.max_by(group, fn {_index, resistor} -> resistor.value end)

          [min, max]
        end)
      # Zip those values with the rest of the results so we can replace them whilst iterating
      |> Stream.zip(resistors)
      # Replace them with resistors marked as included
      |> Enum.map(fn {[{min_index, min}, {max_index, max}], group} ->
        group
          |> Map.put(min_index, %Resistor{min | included: true})
          |> Map.put(max_index, %Resistor{max | included: true})
      end)
  end

  def fetch_from_list(resistors, col, row) do
    with {:ok, group} <- Enum.fetch(resistors, col),
        resistor = %Resistor{} <- Map.get(group, row) do
      {:ok, resistor}
    else
      _ -> :error
    end
  end

  def toggle_in_list(resistors, col, row) do
    case fetch_from_list(resistors, col, row) do
      :error -> {:error, resistors}
      {:ok, resistor} ->
        resistor = %Resistor{resistor | selected: !resistor.selected}
        resistors = List.replace_at(resistors, col, %{Enum.fetch!(resistors, col) | row => resistor})

        {:ok, resistors, resistor}
    end
  end

  def is_solved?(resistors) do
    # The puzzle is solved if all (and only) included resistors are selected
    Enum.all?(resistors, fn group ->
      Enum.all?(group, fn {_, resistor} ->
        resistor.included == resistor.selected
      end)
    end)
  end
end
