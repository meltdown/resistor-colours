defmodule Ui.State do
  @moduledoc """
    `Ui.State` holds the core state for this puzzle, and everything else calls it to find out what to display.
  """

  use GenServer
  alias Ui.State.Resistor
  require Logger

  ## Client API

  @doc """
  Starts the registry.
  """
  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: State)
  end

  @doc """
  Looks up the status of the button.

  Returns `{:ok, status}`, `:error` otherwise.
  """
  def lookup(col, row) do
    GenServer.call(State, {:lookup, col, row})
  end

  @doc """
  Gets the whole grid

  Returns `{:ok, grid}`, `:error` otherwise.
  """
  def lookup() do
    GenServer.call(State, {:lookup})
  end

  @doc """
  Toggles the status of the button.
  """
  def toggle(col, row) do
    GenServer.cast(State, {:toggle, col, row})
  end

  ## Server Callbacks

  def init(:ok) do
    Logger.debug "Initialising state"

    # Long-term this seed will come from the controller, but for now set it randomly
    seed = Enum.take_random('abcdefghijklmnopqrstuvwxyz1234567890', 12)

    # Set up the initial state from the seed
    {:ok, %{resistors: Resistor.list_from_seed("#{seed}", 3, 4)}}
  end

  def handle_call({:lookup}, _from, %{resistors: resistors} = state) do
    {:reply, {:ok, resistors}, state}
  end

  def handle_call({:lookup, col, row}, _from, %{resistors: resistors} = state) do
    {:reply, Resistor.fetch_from_list(resistors, col, row), state}
  end

  def handle_cast({:toggle, col, row}, %{resistors: resistors} = state) do
    resistors = case Resistor.toggle_in_list(resistors, col, row) do
      # The col/row was not found
      {:error, resistors} -> resistors
      # We have a toggled match, so tell everyone to update
      {:ok, resistors, resistor} ->
        # TODO: instead of using channels and pubsub, use channels for everything
        Phoenix.PubSub.broadcast Nerves.PubSub, "resistors", {:refresh}
        UiWeb.Endpoint.broadcast!("leds:lobby", "new_state", %{
          x: col,
          y: row,
          value: resistor.selected,
          solved: Resistor.is_solved?(resistors)
        })

        resistors
    end

    {:noreply, %{state | resistors: resistors}}
  end
end
