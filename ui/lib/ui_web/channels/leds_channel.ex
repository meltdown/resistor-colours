defmodule UiWeb.LEDsChannel do
  use Phoenix.Channel

  def join("leds:lobby", _message, socket) do
    {:ok, socket}
  end

  def join("leds:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_in("toggle_from_ui", %{"x" => x, "y" => y}, socket) do
    Ui.State.toggle(String.to_integer(x), String.to_integer(y))
    {:noreply, socket}
  end
end
