defmodule UiWeb.PageView do
  use UiWeb, :view
  alias Ui.State.Resistor

  def group_sum(group) do
    Enum.reduce(group, 0, fn {_index, resistor}, sum ->
      if resistor.included do
        sum + resistor.value
      else
        sum
      end
    end)
  end
end
