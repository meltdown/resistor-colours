defmodule UiWeb.PageController do
  use UiWeb, :controller

  def index(conn, _params) do
    {:ok, resistors} = Ui.State.lookup()
    render conn, "index.html", resistors: resistors
  end
end
