defmodule Ui.State.ResistorTest do
  use ExUnit.Case
  alias Ui.State.Resistor

  test "single item in seed" do
    seed = "testseed"
    list = Resistor.list_from_seed(seed, 1, 1)

    # `t` is <<116>> in binary, so the value should be 116
    assert [%{0 => %Resistor{included: true, selected: false, value: 116}}] == list
  end

  test "min max calculated correctly" do
    seed = "testseed"
    list = Resistor.list_from_seed(seed, 1, 3)

    # `tes` is <<116, 101, 115>> in binary, so the first and second should be included
    comparator = [%{
      0 => %Resistor{included: true, selected: false, value: 116},
      1 => %Resistor{included: true, selected: false, value: 101},
      2 => %Resistor{included: false, selected: false, value: 115},
    }]

    assert comparator == list
  end

  test "multiple columns" do
    # This tests the whole string, and also makes sure only one of the double "e" and "t" are included
    seed = "tstseedwat"
    list = Resistor.list_from_seed(seed, 3, 3)

    comparator = [
      %{
        0 => %Resistor{included: false, selected: false, value: 116},
        1 => %Resistor{included: true, selected: false, value: 115},
        2 => %Resistor{included: true, selected: false, value: 116},
      },
      %{
        0 => %Resistor{included: true, selected: false, value: 115},
        1 => %Resistor{included: true, selected: false, value: 101},
        2 => %Resistor{included: false, selected: false, value: 101},
      },
      %{
        0 => %Resistor{included: false, selected: false, value: 100},
        1 => %Resistor{included: true, selected: false, value: 119},
        2 => %Resistor{included: true, selected: false, value: 97},
      }
    ]

    assert comparator == list
  end

  test "fetching from list" do
    seed = "tstseedwat"
    list = Resistor.list_from_seed(seed, 3, 3)

    assert {:ok, %Resistor{included: true, selected: false, value: 115}} == Resistor.fetch_from_list(list, 0, 1)
    assert {:ok, %Resistor{included: true, selected: false, value: 97}} == Resistor.fetch_from_list(list, 2, 2)
    assert :error == Resistor.fetch_from_list(list, 3, 2)
    assert :error == Resistor.fetch_from_list(list, 2, 3)
  end

  test "toggling in list" do
    seed = "tstseedwat"
    list = Resistor.list_from_seed(seed, 3, 2)

    {:ok, list, _} = Resistor.toggle_in_list(list, 1, 0)
    {:ok, list, _} = Resistor.toggle_in_list(list, 2, 1)

    assert {:error, ^list} = Resistor.toggle_in_list(list, 3, 1)
    assert {:error, ^list} = Resistor.toggle_in_list(list, 1, 2)

    comparator = [
      %{
        0 => %Resistor{included: true, selected: false, value: 116},
        1 => %Resistor{included: true, selected: false, value: 115},
      },
      %{
        0 => %Resistor{included: true, selected: true, value: 116},
        1 => %Resistor{included: true, selected: false, value: 115},
      },
      %{
        0 => %Resistor{included: true, selected: false, value: 101},
        1 => %Resistor{included: true, selected: true, value: 101},
      }
    ]

    assert comparator == list
  end

  test "checking if solved" do
    seed = "tstseedwat"
    list = Resistor.list_from_seed(seed, 1, 3)

    refute Resistor.is_solved?(list)

    {:ok, list, _} = Resistor.toggle_in_list(list, 0, 0)
    {:ok, list, _} = Resistor.toggle_in_list(list, 0, 1)
    {:ok, list, _} = Resistor.toggle_in_list(list, 0, 2)

    refute Resistor.is_solved?(list)

    {:ok, list, _} = Resistor.toggle_in_list(list, 0, 0)

    assert Resistor.is_solved?(list)
  end
end
