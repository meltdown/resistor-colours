import {Socket} from "phoenix"

let socket = new Socket("/socket", {params: {}})

socket.connect()

let channel = socket.channel("leds:lobby", {})
let buttons = document.querySelectorAll(".js-toggle-resistor")

buttons.forEach(function(input) {
  input.addEventListener("click", event => {
    channel.push("toggle_from_ui", {x: event.target.dataset.x, y: event.target.dataset.y})
  })
})

channel.on("new_state", payload => {
  document.querySelector(`.resistor-${payload.x}${payload.y}`).dataset.selected = payload.value
  document.querySelector('.resistor-solved').dataset.selected = payload.solved
})

channel.join()
  .receive("ok", resp => { console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

export default socket
