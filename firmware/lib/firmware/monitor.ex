defmodule Firmware.Monitor do
  use GenServer
  use Bitwise, only_operators: true
  alias Circuits.GPIO
  require Logger


  @starting_pin 4

  ## Client API

  @doc """
  Starts the registry.
  """
  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: Monitor)
  end

  ## Server Callbacks

  def init(:ok) do
    # Set up the pins to represent a 4 x 3 grid of buttons
    pins = for {pin, index} <- Enum.with_index(@starting_pin..@starting_pin + 11) do
      Logger.info("Starting pin #{pin} as input")
      {:ok, input} = GPIO.open(pin, :input)
      GPIO.set_pull_mode(input, :pullup)

      %{pin: input, debounce: 0, col: trunc(index / 4), row: rem(index, 4)}
    end

    check_buttons()

    {:ok, %{pins: pins}}
  end

  def handle_info(:check_buttons, %{pins: pins} = state) do
    check_buttons()

    pins = for %{pin: pin, debounce: debounce} = input <- pins do
      [debounce, should_toggle] = debounce(debounce, pin)

      if should_toggle do
        Ui.State.toggle(input.col, input.row)
      end

      %{input | debounce: debounce}
    end

    {:noreply, %{state | pins: pins}}
  end

  defp debounce(state, pin) do
    # Debouncing algorithm is from https://hackaday.com/2015/12/10/embed-with-elliot-debounce-your-noisy-buttons-part-ii
    state = ((state <<< 1) ||| (state >>> 8)) &&& 0xFF ||| GPIO.read(pin)

    if (state &&& 0xC3) == 0x03 do
      [0xFF, true]
    else
      [state, false]
    end
  end

  defp check_buttons() do
    Process.send_after(Monitor, :check_buttons, 10) # Check the button state every ten milliseconds
  end
end
