defmodule Firmware.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    opts = [strategy: :one_for_one, name: Firmware.Supervisor]
    children = firmware_workers()
    Supervisor.start_link(children, opts)
  end

  defp firmware_workers do
    if Application.get_env(:firmware, :enable_workers, false),
      do: [Firmware.LEDs, Firmware.Monitor],
      else: []
  end
end
