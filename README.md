This puzzle consists of a grid of "resistors". These are mostly called that as the idea came from trying to read resistor colour codes.

To solve the puzzle, the players must find out which resistors in each column add up to a total (one total per column).

The (extremely rough) sketch below gives an idea for how this should look. Note that only the first column has a screen (with `334` displayed), but in the full puzzle each column will have a screen.

![](./sketch.png)
